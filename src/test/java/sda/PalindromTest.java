package sda;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class PalindromTest {

    private Palindrom charSeries;


    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setup() {

        charSeries = new Palindrom();
    }

    @Test
    public void shouldCheckIfPalindrom() {
        String palindrom = "kajak";

                assertTrue(charSeries.checkIfPalindrom(palindrom));
    }

    @Test
    public void shouldCheckIfNotPalindrom() {
        String notPalindrom = "kajakowski";

        assertFalse(charSeries.checkIfPalindrom(notPalindrom));
    }

    @Test
    public void shouldCheckIfNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Provided argument is incorrect - NULL/empty.");

        charSeries.checkIfPalindrom(null);
    }

    @Test
    public void shouldCheckIfEmptyString() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Provided argument is incorrect - NULL/empty.");

        String emptyInput = "";
        charSeries.checkIfPalindrom(emptyInput);
    }

}
