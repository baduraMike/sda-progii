package calculator;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class StringCalculatorTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void empty_input_returns_zero() {
        String input = "";

        int result = StringCalculator.sum(input);

        Assert.assertEquals(0, result);
    }

    @Test
    public void one_number_in_input_returns_number() {
        String input = "1";

        int result = StringCalculator.sum(input);

        Assert.assertEquals(1, result);
    }

    @Test
    public void two_numbers_in_input_return_sum() {
        String input = "1,2";                           //input syntax - "digit,digit"

        int result = StringCalculator.sum(input);

        Assert.assertEquals(3, result);
    }

    @Test
    public void multiple_numbers_in_input_return_sum() {
        String input = "10,2,3,4";                       //input syntax - digits divided by comma ','

        int result = StringCalculator.sum(input);

        Assert.assertEquals(19, result);
    }

    @Test
    public void nextLine_in_input_return_sum() {
        String input = "1,\n2,3";

        int result = StringCalculator.sum(input);

        Assert.assertEquals(6, result);
    }

    @Test
    public void anyCharacter_in_input_return_sum() {
        String input = "//;\n1;2";

        int result = StringCalculator.sum(input);

        Assert.assertEquals(3, result);
    }

    @Test
    public void minusSeparator_input_return_sum() {
        String input = "//-\n2u-1-2";

        int result = StringCalculator.sum(input);

        Assert.assertEquals(5, result);
    }

    @Test
    public void shouldThrowException() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Negatives not allowed");
        StringCalculator.sum("2--3--3");
    }

    @Test
    public void shouldIgnoreNumbersHigherThan1000() {
        String input = "//;\n1;2;1000;1002;997";

        int result = StringCalculator.sum(input);

        Assert.assertEquals(1000, result);
    }

    @Test
    public void shouldTestAnySeparatorLength() {
        String input = "//[aaa]\n1aaa2aaa3";

        int result = StringCalculator.sum(input);

        Assert.assertEquals(6, result);
    }

    @Test
    public void shouldThrowExceptionNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Input cannot be NULL");

        String nullString = null;
        StringCalculator.sum(nullString);
    }

    @Test
    public void shouldTestMultipleSingularSeparator() {
        String input = "//[%][a]\n1%2a3";

        int result = StringCalculator.sum(input);

        Assert.assertEquals(6, result);
    }

    @Test
    public void shouldTestMultipleSeparatorsSequences() {
        String input = "//[%][a]ghgjfd1\n\n\n%2aaaaa3";

        int result = StringCalculator.sum(input);

        Assert.assertEquals(6, result);
    }

}
