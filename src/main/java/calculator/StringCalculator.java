package calculator;


public class StringCalculator {


    public static int sum(String input) {

        String inputNoNewLines;
        int sum = 0;

        if (input == null) {
            throw new IllegalArgumentException("Input cannot be NULL");
        } else if (input.matches("[\\s\\S]*(-\\d)[\\s\\S]*") && !input.matches("[\\s\\S]*(\\d-\\d)[\\s\\S]*")) {

            /* "-digit" - negative number ; "digit-digit" - separator; "digit--digit" - separator and negative number */

            throw new IllegalArgumentException("Negatives not allowed");
        } else {
            inputNoNewLines = input.replaceAll("\\D", ",");
        }

        if (inputNoNewLines.equals("")) {
            return 0;
        } else {

            String[] stringArrayByRegex = inputNoNewLines.split(",");

            for (String e : stringArrayByRegex) {
                while (!e.equals("") && Integer.parseInt(e) < 1000) {
                    sum += Integer.parseInt(e);
                    break;

                }
            }

        }

        return sum;
    }

}
