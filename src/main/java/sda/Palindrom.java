package sda;

public class Palindrom {


    public static boolean checkIfPalindrom(String charSeries) {

        if (charSeries==null || charSeries.length()==0){
            throw new IllegalArgumentException("Provided argument is incorrect - NULL/empty.");
        }

        String inputString = charSeries;

        int left = 0;
        int right = inputString.length() - 1;

        while (left < right) {

            if (inputString.charAt(left) != inputString.charAt(right)) {
                return false;
            }
            left++;
            right--;
        }
        return true;
    }

}


